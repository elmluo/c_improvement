#include<stdio.h>
#include<string.h>
#include<stdlib.h>

/*
获取子串的个数do-while。
strstr(母串，子串)
*/
#if 0
int main(void) 
{
	char *str = "12345chaogege23423423chaogege213424chaogegejiowejfoiwchaogege123chaogege";
	//				   ↑
	char *sub_str = "chaogege";
	char *p = NULL;
	int count = 0; // 统计个数

	// do-while
	do {
		p = strstr(p, sub_str);  // 找到一个之后从p开始接着查
		if (p!=NULL) {
			// 找到一个
			count++;
			p++;
		}
	} while (p != NULL);


	prinf("%d", count);
	return 0;
}
#endif