#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/*
C语言中没有字符串类型， 用连续的字符数组 后面加上一个‘\0’
*/

int main(void)
{
	printf("123456");
	//char str1[] = "abcd"; // 5
	char str1[] = { 'a', 'b', 'c', 'd', '\0' }; // 字符串
	//char str1[] = { 'a', 'b', 'c', 'd' };  // 字符数组
	char *str2 = "abcd";  // 4
	char str3[128] = { 'x', 'y', 'z' };	// 默认填充0

	printf("str1:%s\n", str1);
	printf("str3:%s\n", str3);
	int i = 0;
	for ( i = 0; i < 128; i++)
	{
		printf("%d ", str3[i]);
	}
	printf("\n");
	printf("-------------------------\n");
	printf("strlen(str2):%d\n", strlen(str2)); // 4
	printf("strlen(str3):%d\n", strlen(str3)); // 3
	printf("sizeof(str2):%d\n", sizeof(str2)); // 4
	printf("sizeof(str3):%d\n", sizeof(str3)); // 128


	// 取字符串中的元素，通过数组下标
	int len = strlen(str1);
	for ( i = 0; i < len; i++)
	{
		printf("%c\n", str1[i]); 
	}

	// 通过指针
	char *p = NULL;
	p = str1;
	for ( i = 0; i < len; i++, p++)
	{
		printf("%c\n", *p);
	}

	// 指针 和 数组名的区别
	// 通过不断改变指针的方向来索引数组，会比直接用数组下标，速度要慢。
	// 有不断的修改指针的方向 就是修改内存，
	
	/*
	p++

	str1++ 为什么不可以
		数组名方向不可以改变
		数组名是一个常量指针（方向不可以改变）
		栈是操作系统开辟和回收，是根据数组名字来找到一块连续内存的首地址，如果首地址变量改变了。将无法准确的回收。

		数组和指针
			数组一般在栈上开辟内存
			指针一般在堆上
	*/
	/*
	编译器会将 数组转成指针
		p[i] ==> p[0 + i] ==> *(p + i)
		p[i][j] ==> p[i][0 + j] ==> *(p[i] + j) ==> *(p[0 + i] + j) ==> *(*(p + i) + j])
	*/


	getchar();
	return 0;
}