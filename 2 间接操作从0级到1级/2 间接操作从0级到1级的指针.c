#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


get_file_len(int file_len) 
{
	file_len = 666666;
}

// 如果不使用指针，想要将子函数中的数据传递出来，那么一定要有返回值，发生值拷贝。
get_file_len2()
{
	int file_len = 0;
	file_len = 3000;
	return file_len;
}

// 通过指针在函数的内部，间接修改函数外部的变量。
get_file_len3(int *p) 
{
	*p = 30;
	return 0;
}

int main(void) 
{
	int file_len = 0;
	//get_file_len(file_len);
	//file_len = get_file_len2();
	get_file_len3(&file_len);
	printf("file_len的值：%d", file_len);
	getchar();
	return 0;
}