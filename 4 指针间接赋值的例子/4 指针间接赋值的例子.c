#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 空的define 类型，用于代码的注释
#define OUT   // 定义为函数的输出参数
#define IN    // 定义为函数的输入参数

// 在堆中开辟两块内存，提供给main函数使用
int get_mem(OUT char **pp1, OUT char **pp2, OUT int *len_p1, OUT int *len_p2, IN int len1, IN int len2) 
{
	char *p1 = NULL;
	char *p2 = NULL;
	int temp_len1 = 0;
	int temp_len2 = 0;

	p1 = (char*)malloc(len1);
	if (p1 == NULL) {
		fprintf(stderr, "malloc p1 err\n");
		return -1;
	}
	memset(p1, 0, len1);

	p2 = (char*)malloc(len2);
	if (p2 == NULL) {
		fprintf(stderr, "malloc p2 err\n");
		return -1;
	}
	memset(p2, 0, len2);

	strcpy(p1, "123465789");
	temp_len1 = strlen(p1);

	strcpy(p2, "abcdefghijklmn");
	temp_len2 = strlen(p2);

	// 到此 说明以上程序没有问题了；
	*pp1 = p1;
	*pp2 = p2;
	*len_p1 = temp_len1;
	*len_p2 = temp_len2;

	return 0;
}
/*
对外提供一个 释放内存的接口
*/
void free_mem(char *p1, char *p2) 
{
	if (p1 != NULL) {
		free(p1);
	}
	if (p2 != NULL) {
		free(p2);
	}

}

void free_mem2(char **pp1, char **pp2) {
	char *p1 = NULL;
	char *p2 = NULL;
	p1 = *pp1;
	p2 = *pp2;
	if (p1 != NULL) {
		free(p1);
	}
	if (p2 != NULL) {
		free(p2);
	}
	*pp1 = NULL;
	*pp2 = NULL;
}

int main(void) 
{
	char *p1 = NULL; // 第一块内存首地址
	char *p2 = NULL; // 
	int len1 = 0;   // 第一块内存长度
	int len2 = 0;
	int result = 0;
	result = get_mem(&p1, &p2, &len1, &len2, 200, 400);
	if (result != 0) {
		fprintf(stderr, "get mem error: %d \n", result);
	};
	// 对内存的操作
	printf("p1: %s[%d], p2:%s[%d]\n", p1, len1, p2, len2);
	/*if (p1 != NULL) {
		free(p1);
		p1 = NULL;
	}
	if (p2 != NULL) {
		free(p2);
		p2 = NULL;
	}*/

	//free_mem(p1, p2);
	//p1 = NULL;
	//p2 = NULL;

	free_mem2(&p1, &p2);

	return 0;
}