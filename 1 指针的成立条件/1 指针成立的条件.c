#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
指正成立的条件	
	1. 要有两个变量int num = 0; int *p = null;
	2. 建立关系 p = &num
	3. 间接赋值 *p = 2
*/

int change_value(int *p) {
	*p = 41;
}

int main(void) {
	printf("超哥哥你好");
	int num = 0;
	int *p = NULL;
	change_value(&num);
	
	getchar();
	return 0;
}