#include <stdio.h>
#include <stdlib.h>


// int **p 表示，首先*p表示p是指针变量，而且只存放int *变量地址
void change_pointer(int **p) {
	*p = (int*)0x12345678;
}

int main(void) {
	int *p = NULL;
	change_pointer(&p);

	getchar();
	return 0;
}

#if 0
int main(void) {
	printf("abc");
	int i = 10;
	int *p = &i;
	int **q = &p;
	int ***r = &q;
	printf("i=%d/n", ***r);

	getChar();
	return 0;
}
#endif