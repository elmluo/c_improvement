#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
src 源字符串
dst 目标地址
*/

#define OUT
#define IN

int str_copy1(char *dst, char *src)
{
	
	for ( ; *src != '\0'; src++,dst++)
	{
		*dst = *src;
	}
	*dst = '\0';
	return 0;
}

int str_copy2(char *dst, char *src) 
{
	for (; *src != '\0';) {
		*dst++ = *src++;
	}
	*dst = '\0';
	return 0;
}


int str_copy3(char *dst, char*src)
{
	for (; (*dst = *src) != '\0'; src++, dst++) {

	}
	return 0;
}

int str_copy4(char *dst, char*src)
{
	while ((*dst++ = *src++));
	return 0;
}

int str_copy(/*OUT*/ char *dst,/*IN*/ const char *src)
{
	int rtn = 0; // 返回变量, 用的熟练的话可以用goto语句。

	// 先对参数进行合法判断
	if (dst == NULL || src == NULL) { 
		fprintf(stderr, "dst == NULL || src == NULL\n");
		rtn = -1;
		goto END;
	}
	// 习惯使用临时变量，养成习惯，先赋值为空或者0，
	char *p_dst = NULL;
	char *p_src = NULL;

	for (p_dst = dst, p_src = (char *)src; *p_dst++ = *p_src++; ) {
	}
	printf("in str_copy: dst=%s\n", dst);

	END: // 标签这里使用：
	return rtn;
}

int main(void) {
	char *str = "12345678";
	char dst[128] = { 0 };
	str_copy(dst, str);
	printf("%s", dst);
	getchar();
	return 0;
}